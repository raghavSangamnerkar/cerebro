package com.raghav.playground.cerebro.speech.record

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.text.TextUtils
import android.util.Log
import com.raghav.playground.cerebro.util.getFilePath
import java.io.File
import kotlin.properties.Delegates

/**
 * Responsible to log all the user records as a statement or
 * reply to a particular statement
 *
 * @author raghav.sangamnerkar
 */
class RecordManager {

    var mContext: Context? = null
    var mode: Int by Delegates.observable(MODE_DEFAULT) { d, old, new ->
        prefix = if (new == MODE_QUESTION) PREFIX_QUESTION else PREFIX_ANSWER
    }

    var chat: MutableLiveData<MutableList<String>>

    var handlerThread: HandlerThread? = null
    var handler: Handler? = null


    private var prefix: String? = null

    companion object {

        val PREFIX_QUESTION: String = "QUE::"
        val PREFIX_ANSWER: String = "ANS::"
        const val CONV_LOG = "conv_log.txt"
        const val MODE_DEFAULT: Int = -1
        const val MODE_QUESTION: Int = 0
        const val MODE_ANSWER = 1

        @Volatile
        private var INSTANCE: RecordManager? = null

        fun getInstance(context: Context): RecordManager {
            INSTANCE ?: RecordManager(context).also { INSTANCE = it }

            return INSTANCE as RecordManager;
        }
    }

    constructor(context: Context) {
        mContext = context
        chat = MutableLiveData()
        initRecord()
        initRecordLogQueue()
        mode = MODE_QUESTION
    }

    /**
     * Initialize the logging queue
     */
    private fun initRecordLogQueue() {
        handlerThread = HandlerThread(RecordManager::class.java.name)
        handlerThread?.start()
        handler = Handler(handlerThread?.looper)
    }


    /**
     * Reads the content from the file
     */
    fun readContentFromFile() {

        handler?.post(Runnable {
            val content: List<String> = File(getFilePath(mContext)).readLines()
            chat.postValue(content.toMutableList())
            content.forEach {
                Log.v("Devdas", it)
            }
        })

    }

    /**
     * If the record file does not exist, create one
     */
    private fun initRecord() {
        val file = File(getFilePath(mContext))
        if (!file.exists())
            file.setReadable(true, false)
            file.createNewFile()
    }

    /**
     * Record the input provided by the user
     */
    fun recordInput(record: String) {
        if (mode == MODE_DEFAULT || TextUtils.isEmpty(record))
            return

        handler?.post(Runnable { File(getFilePath(mContext)).appendText(prefix + record + "\n")

            //Add data to local live data, so that observers get the update
            var chatList: MutableList<String>? = chat.value
            chatList?.add(prefix + record)
            chat.postValue(chatList)
            if (mode == MODE_QUESTION)
                mode = MODE_ANSWER
            else
                mode = MODE_QUESTION
        })
    }


    fun getChat(): LiveData<MutableList<String>> {
        return chat
    }



}
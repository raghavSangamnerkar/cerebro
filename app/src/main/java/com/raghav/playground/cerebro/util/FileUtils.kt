package com.raghav.playground.cerebro.util

import android.content.Context
import com.raghav.playground.cerebro.speech.record.RecordManager


 fun getFilePath(context :Context?):String{
    return context?.getExternalFilesDir(null)?.path +"/"+ RecordManager.CONV_LOG
}

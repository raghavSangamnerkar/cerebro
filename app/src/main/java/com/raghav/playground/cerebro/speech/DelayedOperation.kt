package com.raghav.playground.cerebro.speech

import android.R.string.cancel
import android.content.Context
import android.os.Handler
import android.os.Looper.getMainLooper
import android.util.Log
import java.util.*


/**
 * Created by raghav.sangamnerkar on 9/27/18.
 */
class DelayedOperation(context: Context?, tag: String, delayInMilliseconds: Long) {
    private val LOG_TAG = DelayedOperation::class.java.simpleName

    interface Operation {
        fun onDelayedOperation()
        fun shouldExecuteDelayedOperation(): Boolean
    }

    private var mDelay: Long = -1
    private var mOperation: Operation? = null
    private var mTimer: Timer? = null
    private var started: Boolean = false
    private var mContext: Context? = null
    private var mTag: String? = null

    init {
        if (context == null) {
            throw IllegalArgumentException("Context is null")
        }

        if (delayInMilliseconds <= 0) {
            throw IllegalArgumentException("The delay in milliseconds must be > 0")
        }

        mContext = context
        mTag = tag
        mDelay = delayInMilliseconds
        Log.d(LOG_TAG, "created delayed operation with tag: " + mTag)
    }

    fun start(operation: Operation?) {
        if (operation == null) {
            throw IllegalArgumentException("The operation must be defined!")
        }

        Log.d(LOG_TAG, "starting delayed operation with tag: " + mTag)
        mOperation = operation
        cancel()
        started = true
        resetTimer()
    }

    fun resetTimer() {
        if (!started) return

        if (mTimer != null) mTimer!!.cancel()

        Log.d(LOG_TAG, "resetting delayed operation with tag: " + mTag)
        mTimer = Timer()
        mTimer!!.schedule(object : TimerTask() {
            override fun run() {
                if (mOperation!!.shouldExecuteDelayedOperation()) {
                    Log.d(LOG_TAG, "executing delayed operation with tag: " + mTag)
                    Handler(mContext?.mainLooper).post(Runnable { mOperation!!.onDelayedOperation() })
                }
                cancel()
            }
        }, mDelay)
    }

    fun cancel() {
        if (mTimer != null) {
            Log.d(LOG_TAG, "cancelled delayed operation with tag: " + mTag)
            mTimer!!.cancel()
            mTimer = null
        }

        started = false
    }
}
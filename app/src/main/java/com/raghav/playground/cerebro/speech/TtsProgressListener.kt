package com.raghav.playground.cerebro.speech

import android.content.Context
import android.os.Handler
import android.os.Looper.getMainLooper
import android.speech.tts.UtteranceProgressListener
import java.lang.ref.WeakReference


/**
 * @author raghav.sangamnerkar
 */
class TtsProgressListener(context: Context, private val mTtsCallbacks: MutableMap<String, TextToSpeechCallback>) : UtteranceProgressListener() {

    private val contextWeakReference: WeakReference<Context>

    init {
        contextWeakReference = WeakReference(context)
    }

    override fun onStart(utteranceId: String) {
        val callback = mTtsCallbacks[utteranceId]
        val context = contextWeakReference.get()

        if (callback != null && context != null) {
            Handler(context!!.getMainLooper()).post(Runnable { callback.onStart() })
        }
    }

    override fun onDone(utteranceId: String) {
        val callback = mTtsCallbacks[utteranceId]
        val context = contextWeakReference.get()
        if (callback != null && context != null) {
            Handler(context!!.getMainLooper()).post(Runnable {
                callback.onCompleted()
                mTtsCallbacks.remove(utteranceId)
            })
        }
    }

    override fun onError(utteranceId: String?) {
        val callback = mTtsCallbacks[utteranceId]
        val context = contextWeakReference.get()

        Handler(context!!.getMainLooper()).post(Runnable {
            callback!!.onError()
            mTtsCallbacks.remove(utteranceId)
        })
    }

    override fun onError(utteranceId: String?, errorCode: Int) {
        val callback = mTtsCallbacks[utteranceId]
        val context = contextWeakReference.get()

        Handler(context!!.getMainLooper()).post(Runnable {
            callback!!.onError()
            mTtsCallbacks.remove(utteranceId)
        })
    }
}

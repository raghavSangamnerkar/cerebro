package com.raghav.playground.cerebro.speech

/**
 * @author raghav.sangamnerkar
 */
interface TextToSpeechCallback {
    fun onStart()
    fun onCompleted()
    fun onError()
}
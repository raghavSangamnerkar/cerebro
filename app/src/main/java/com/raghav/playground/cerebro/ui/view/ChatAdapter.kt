package com.raghav.playground.cerebro.ui.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raghav.playground.cerebro.R
import com.raghav.playground.cerebro.speech.record.RecordManager

class ChatAdapter(context:Context, chatList: MutableList<String>): RecyclerView.Adapter<ChatViewHolder>() {


    val ITEM_SENT:Int = 0
    val ITEM_RECEIVED:Int = 1

    var context: Context? = null
    lateinit var chat:MutableList<String>

    init {
        this.context = context
        chat = chatList
    }


    fun updateChatData(chatList: MutableList<String>?){
        chat = chatList!!
        notifyDataSetChanged()
    }



    override fun getItemCount(): Int {
        return chat.size
    }

    override fun onBindViewHolder(p0: ChatViewHolder, p1: Int) {
        val record:String = trimRecord(chat.get(p1))
        p0.body.text = record
    }

    override fun getItemViewType(position: Int): Int {
        return if (isQuestion(chat.get(position))) ITEM_RECEIVED else ITEM_SENT
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ChatViewHolder {
        var layoutView: View? = null
        if(ITEM_RECEIVED == p1){
            layoutView = LayoutInflater.from(p0.getContext())
                    .inflate(R.layout.item_message_received, p0, false)
        }else{
            layoutView = LayoutInflater.from(p0.getContext())
                    .inflate(R.layout.item_message_sent,p0, false)
        }
        return ChatViewHolder(layoutView)
    }

    private fun trimRecord(record:String): String {
        if(isQuestion(record))
            return record.replace(RecordManager.PREFIX_QUESTION,"", false)
        else
            return record.replace(RecordManager.PREFIX_ANSWER,"", false)
    }


    private fun isQuestion(record:String): Boolean {
        return record.startsWith(RecordManager.PREFIX_QUESTION,true)
    }
}
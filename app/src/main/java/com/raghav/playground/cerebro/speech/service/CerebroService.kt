package com.raghav.playground.cerebro.speech.service

import android.Manifest
import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Build
import android.os.IBinder
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.raghav.playground.cerebro.App
import com.raghav.playground.cerebro.R
import com.raghav.playground.cerebro.SampleSingleton
import com.raghav.playground.cerebro.speech.Speech
import com.raghav.playground.cerebro.speech.SpeechDelegate
import com.raghav.playground.cerebro.speech.exception.GoogleVoiceTypingDisabledException
import com.raghav.playground.cerebro.speech.exception.SpeechRecognitionNotAvailable
import com.raghav.playground.cerebro.speech.record.RecordManager
import com.tbruyelle.rxpermissions.RxPermissions
import java.util.*

/**
 * A foreground service which continuously listens to user input
 *
 * @author raghav.sangamnerkar
 */
class CerebroService : Service(), SpeechDelegate, Speech.stopDueToDelay {

    lateinit var delegate: SpeechDelegate

    override fun onCreate() {
        super.onCreate()
        startForeground(12312, getNotification())
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        RecordManager.getInstance(this).mode = RecordManager.MODE_QUESTION
        RecordManager.getInstance(this).readContentFromFile()
        SampleSingleton.instance.b++
        Log.v("ajay", "onStartCommand :: " + SampleSingleton.instance.b)
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                (Objects.requireNonNull(
                        getSystemService(Context.AUDIO_SERVICE)) as AudioManager).setStreamMute(AudioManager.STREAM_SYSTEM, true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        Speech.initSpeech(this)
        delegate = this
        Speech.getInstance().setListener(this)

        if (Speech.getInstance().isListening()) {
            Speech.getInstance().stopListening()
            muteBeepSoundOfRecorder()
        } else {
            System.setProperty("rx.unsafe-disable", "True")
            RxPermissions.getInstance(this).request(Manifest.permission.RECORD_AUDIO).subscribe({ granted ->
                if (granted) { // Always true pre-M
                    try {
                        Speech.getInstance().stopTextToSpeech()
                        Speech.getInstance().startListening(this)
                    } catch (exc: SpeechRecognitionNotAvailable) {
                        //showSpeechNotSupportedDialog();

                    } catch (exc: GoogleVoiceTypingDisabledException) {
                        //showEnableGoogleVoiceTyping();
                    }

                } else {
                    Toast.makeText(this, R.string.permission_required, Toast.LENGTH_LONG).show()
                }
            })
            muteBeepSoundOfRecorder()
        }
        return Service.START_STICKY
    }

    private fun getNotification(): Notification {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification = Notification.Builder(applicationContext, (application as App).channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Active")
                    .setContentText("Listening to conversation...")
//                    .setContentIntent(PendingIntent.getActivity(applicationContext, Random().nextInt(),
//                            Intent(applicationContext, HomeActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT))
                    .build()
            return notification

        } else {
            val notification = Notification.Builder(applicationContext)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Active")
                    .setContentText("Listening to conversation...")
//                    .setContentIntent(PendingIntent.getActivity(applicationContext, Random().nextInt(),
//                            Intent(applicationContext, HomeActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT))
                    .build()
            return notification
        }
    }


    override fun onSpeechRmsChanged(value: Float) {
    }


    override fun onStartOfSpeech() {
    }

    override fun onSpeechPartialResults(results: List<String>) {
        for (partial in results) {
            Log.d("Result", partial + "")
        }
    }

    override fun onSpeechResult(result: String) {
        Log.d("Result", result + "")
        if (!TextUtils.isEmpty(result)) {
            RecordManager.getInstance(this).recordInput(result)
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSpecifiedCommandPronounced(event: String) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                (Objects.requireNonNull(
                        getSystemService(Context.AUDIO_SERVICE)) as AudioManager).setStreamMute(AudioManager.STREAM_SYSTEM, true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (Speech.getInstance().isListening()) {
            Speech.getInstance().stopListening()
            muteBeepSoundOfRecorder()
        } else {
            RxPermissions.getInstance(this).request(Manifest.permission.RECORD_AUDIO).subscribe { granted ->
                if (granted!!) { // Always true pre-M
                    try {
                        Speech.getInstance().stopTextToSpeech()
                        Speech.getInstance().startListening( this)
                    } catch (exc: SpeechRecognitionNotAvailable) {
                        //showSpeechNotSupportedDialog();

                    } catch (exc: GoogleVoiceTypingDisabledException) {
                        //showEnableGoogleVoiceTyping();
                    }

                } else {
                    Toast.makeText(this, R.string.permission_required, Toast.LENGTH_LONG).show()
                }
            }
            muteBeepSoundOfRecorder()
        }
    }


    /*override fun onTaskRemoved(rootIntent: Intent) {
        //Restarting the service if it is removed.
        val service = PendingIntent.getService(applicationContext, Random().nextInt(),
                Intent(applicationContext, CerebroService::class.java), PendingIntent.FLAG_ONE_SHOT)

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, service)
        super.onTaskRemoved(rootIntent)
    }*/

    /**
     * Function to remove the beep sound of voice recognizer.
     */
    private fun muteBeepSoundOfRecorder() {
        val amanager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (amanager != null) {
            amanager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_MUTE,-1)
            amanager.adjustStreamVolume(AudioManager.STREAM_ALARM,  AudioManager.ADJUST_MUTE,-1)
            amanager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE,-1)
            amanager.adjustStreamVolume(AudioManager.STREAM_RING,  AudioManager.ADJUST_MUTE,-1)
            amanager.adjustStreamVolume(AudioManager.STREAM_SYSTEM,  AudioManager.ADJUST_MUTE,-1)
        }
    }



    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }


}

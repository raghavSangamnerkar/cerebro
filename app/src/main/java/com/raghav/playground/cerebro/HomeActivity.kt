package com.raghav.playground.cerebro

import android.arch.lifecycle.Observer
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.raghav.playground.cerebro.speech.service.CerebroService
import kotlinx.android.synthetic.main.activity_home.*
import android.view.View
import java.io.File
import android.os.StrictMode
import android.support.v7.widget.LinearLayoutManager
import com.raghav.playground.cerebro.util.getFilePath
import com.raghav.playground.cerebro.speech.record.RecordManager
import com.raghav.playground.cerebro.ui.view.ChatAdapter


class HomeActivity : AppCompatActivity() {

    var adapter: ChatAdapter? = null
    var handler: Handler? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
//                message.setText(R.string.title_home)
                send_mail.hide()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                send_mail.show()
//                message.setText(R.string.title_dashboard)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                send_mail.show()
//                message.setText(R.string.title_notifications)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        send_mail.hide()
        adapter = ChatAdapter(this, ArrayList())
        handler = Handler(Looper.getMainLooper())

        if (RecordManager.getInstance(this).mode == RecordManager.MODE_QUESTION){
            mode.text = "Q"
        } else if (RecordManager.getInstance(this).mode == RecordManager.MODE_ANSWER){
            mode.text = "A"
        }

        chat_view.layoutManager = LinearLayoutManager(this)
        chat_view.adapter = adapter

        send_mail.setOnClickListener { v: View? ->
            sendMail()
        }


        RecordManager.getInstance(this).getChat()
                .observe(this, Observer {
                    adapter?.updateChatData(it)
                    handler?.postDelayed(Runnable {
                        it?.size?.let { it1 ->
                            if (it1 > 0)
                            chat_view.smoothScrollToPosition(it1 - 1)
                        }
                        mode.text = if (RecordManager.getInstance(this).mode == RecordManager.MODE_QUESTION) "Q" else "A"

                    }, 100)

                })


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        startService(Intent(applicationContext, CerebroService::class.java))
    }

    fun sendMail() {
        val filelocation = File(getFilePath(this))
        val path = Uri.fromFile(filelocation)
        val emailIntent = Intent(Intent.ACTION_SEND)
        // set the type to 'email'
        emailIntent.type = "vnd.android.cursor.dir/email"
        val to = arrayOf("raghavsangamnerkar@gmail.com")
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
        // the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, path)
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Conversation Log")
        startActivityForResult(Intent.createChooser(emailIntent, "Send email..."), 123)
    }


}
